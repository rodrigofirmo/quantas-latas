# Quantas latas?

Descubra quantas latas de tinta serão necessárias para pintar uma sala.

## Instalando

Para instalar, siga os passos:
1. Clone o repositório na sua máquina
2. Abra a pasta clonada em seu editor de código
3. No terminal, use o comando **yarn** para instalar as dependências necessárias
```
yarn
```

## Rodando o projeto

Após a instalação, rode o projeto com o comando **yarn start**.
```
yarn start
```
O projeto poderá ser acessado em seu navegador, pela url http://localhost:3000/
