import { createGlobalStyle } from "styled-components";

const kreon = require("../assets/fonts/Kreon-VariableFont_wght.ttf");
const background = require("../assets/img/background-mobile.jpeg");
const backgroundDesktop = require("../assets/img/background.jpg");
const backgroundWide = require("../assets/img/background-wide.jpg");

export const Reset = createGlobalStyle`
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
`;

export const RootVariables = createGlobalStyle`
@font-face {
	font-family: "Kreon";
	src: url(${kreon});
}

:root{
	//THEME COLORS
	--color-primary: #0C247A;
    --color-secondary: #1849FA;

	//GENERAL COLORS
	--color-white: #FFFFFF;

	//FONTS
	--kreon: "Kreon", serif;

	//TYPOGRAPHY
	--title-24: bold 1.5rem var(--kreon);
	--text-14: lighter 0.85rem var(--kreon);
    --text-12: normal 0.75rem var(--kreon);
}
`;

export const GlobalStyles = createGlobalStyle`
    body { 
        font: var(--text-12);
        background-image: url(${background});
        color: var(--color-secondary);
        min-height: 100vh;
		@media (min-width: 768px) {
		background-image: url(${backgroundDesktop});
    }
	@media (min-width: 1024px) {
		background-image: url(${backgroundWide});
    }

    button {
        cursor: pointer;
    }
	
  }
`;
