import styled from "styled-components";

export const Container = styled.header`
  position: fixed;
  width: 100vw;
  height: 60px;
  background-color: var(--color-primary);
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  color: white;
`;

export const TitleDiv = styled.div`
  display: flex;
  padding-left: 5%;
`;
export const MenuDiv = styled.div`
  font-size: 30px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  padding-right: 20px;
`;

export const TitleH3 = styled.h3`
  font-size: 28px;
  color: white;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;
