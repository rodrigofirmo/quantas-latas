import { Container, MenuDiv, TitleDiv, TitleH3 } from "./style";

const Header = () => {
  return (
    <Container>
      <TitleDiv>
        <TitleH3>Quantas Latas?</TitleH3>
      </TitleDiv>
      <MenuDiv></MenuDiv>
    </Container>
  );
};

export default Header;
