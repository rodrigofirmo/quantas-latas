import styled from "styled-components";

export const Container = styled.div`
  width: 100vw;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding-top: 80px;
  color: white;
  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: space-between;
  }
`;

export const InstructionsBox = styled.div`
  width: 90%;
  height: 480px;
  background-color: green;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  color: white;
  background-color: var(--color-secondary);
  margin: 10px auto;
  border-radius: 20px;
  padding: 10px;
  @media (min-width: 768px) {
    margin: 5%;
  }
`;

export const InstructionsTop = styled.div`
  font-size: 22px;
  display: flex;
  text-align: center;
  margin: 10px auto;
  @media (min-width: 768px) {
    font-size: 26px;
  }
`;

export const InstructionsBottom = styled.div`
  font-size: 14px;
  margin-top: 10px;
  margin-left: 5px;
  @media (min-width: 768px) {
  }
`;

export const InputsBox = styled.div`
  width: 90%;
  height: 480px;
  background-color: blue;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  color: white;
  background-color: var(--color-secondary);
  margin: 30px auto;
  border-radius: 20px;
  opacity: 90%;
  @media (min-width: 768px) {
    margin: 5%;
  }
`;

export const InputsTop = styled.div`
  height: 75px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-self: center;
  align-items: center;
  text-align: center;
  font-size: 14px;
  span {
    margin-top: 25px;
  }
`;

export const Form = styled.form`
  width: 90%;
  padding: 0px 10px 20px 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 10px auto;
`;

export const InputField = styled.input`
  width: 35px;
  height: 35px;
  font-size: 16px;
  border-radius: 10px;
  align-items: center;
  text-align: center;
  border: none;
  margin: 0px 2px;
  :focus {
  }
`;

export const InputsDiv = styled.div`
  width: 90%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;

export const Text = styled.span`
  font-size: 16px;
`;

export const Button = styled.button`
  width: 150px;
  height: 40px;
  background-color: var(--color-primary);
  color: white;
  border-radius: 5px;
  border: none;
  font-size: 24px;
  box-shadow: rgba(0, 0, 0, 0.15) 0px 3px 3px 0px;
  margin-top: 20px;
  :hover {
    border: 2px solid var(--color-secondary);
    opacity: 80%;
  }
`;

export const ResultDiv = styled.div`
  height: 150px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  font-size: 16px;
  padding: 0px 20px;
  text-align: center;
  @media (min-width: 768px) {
    font-size: 20px;
  }
`;

export const Footer = styled.footer``;
