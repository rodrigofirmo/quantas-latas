import Header from "../../components/Header";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import { toast } from "react-toastify";
import {
  Container,
  InstructionsBox,
  InputsBox,
  InstructionsTop,
  InstructionsBottom,
  InputField,
  InputsDiv,
  Text,
  Form,
  Button,
  ResultDiv,
  InputsTop,
  Footer,
} from "./style";

const Home = () => {
  const formSchema = yup.object().shape({
    doors1: yup.number(),
    windows1: yup.number(),
    doors2: yup.number(),
    windows2: yup.number(),
    doors3: yup.number(),
    windows3: yup.number(),
    doors4: yup.number(),
    windows4: yup.number(),
  });

  const { register, handleSubmit } = useForm({
    resolver: yupResolver(formSchema),
  });

  const [totalMeters, setTotalMeters] = useState(0);
  const [totalCans, setTotalCans] = useState("");

  const handleSubmitArea = async (data) => {
    let doorArea = 0.8 * 1.9;
    let windowArea = 2 * 1.2;

    // Replace commas with dots
    Object.keys(data).forEach(function (key, index) {
      data[key] = data[key].toString();
      data[key] = parseFloat(data[key].replace(/,/g, "."));
    });

    let {
      height1,
      width1,
      doors1,
      windows1,
      height2,
      width2,
      doors2,
      windows2,
      height3,
      width3,
      doors3,
      windows3,
      height4,
      width4,
      doors4,
      windows4,
    } = data;

    const errorMessage = (message) => {
      toast.error(message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        progress: undefined,
      });
    };

    const checkWallSizeLimits = (height, width) => {
      if (height * width > 50 || height < 1 || width < 1) {
        return false;
      }
    };

    const checkDoorsAndWindowsArea = (height, width, doors, windows) => {
      if (doorArea * doors + windowArea * windows > (height * width) / 2) {
        return false;
      }
    };

    const checkWallHeight = (height, doors) => {
      if (doors > 0 && height < 2.2) {
      } else {
        return true;
      }
    };

    const wallSquareMeters = (height, width, doors, windows) => {
      if (checkWallSizeLimits(height, width) === false) {
        errorMessage("Verifique se as medidas estão dentro dos limites");
      } else if (
        checkDoorsAndWindowsArea(height, width, doors, windows) === false
      ) {
        errorMessage(
          "O total de área das portas e janelas deve ser no máximo 50% da área de parede"
        );
      } else if (checkWallHeight(height, doors) === false) {
        errorMessage(
          "A altura de uma parede com portas deve ser maior que 2,20m"
        );
      } else {
        return height * width - (doors * doorArea + windows * windowArea);
      }
    };

    const totalArea =
      wallSquareMeters(height1, width1, doors1, windows1) +
      wallSquareMeters(height2, width2, doors2, windows2) +
      wallSquareMeters(height3, width3, doors3, windows3) +
      wallSquareMeters(height4, width4, doors4, windows4);

    setTotalMeters(totalArea);

    const cans = (totalArea) => {
      let liters = totalArea / 5;
      let cans18 = 0;
      let cans3_6 = 0;
      let cans2_5 = 0;
      let cans0_5 = 0;

      while (liters > 0) {
        if (liters >= 18) {
          cans18 = parseInt(liters / 18);
          liters = liters % 18;
        }
        if (liters > 17.5) {
          cans18 = 1;
          liters = 0;
        }
        if (liters >= 3.6) {
          cans3_6 = parseInt(liters / 3.6);
          liters = liters % 3.6;
        }
        if (liters >= 2.5) {
          cans2_5 = parseInt(liters / 2.5);
          liters = liters % 2.5;
        }
        if (liters > 2) {
          cans2_5 = 1;
          liters = 0;
        }
        if (liters > 0) {
          cans0_5 = Math.ceil(liters / 0.5);
          liters = 0;
        }
      }
      return `${cans18} lata(s) de 18L, ${cans3_6} lata(s) de 3.6L, ${cans2_5} lata(s) de 2.5L e ${cans0_5} lata(s) de 0.5L`;
    };
    setTotalCans(cans(totalArea));
  };

  return (
    <>
      <Header />
      <Container>
        <InstructionsBox>
          <InstructionsTop>
            Calcule quantas latas de tinta são necessárias para pintar uma sala.
            Insira a altura e largura de cada parede e o número de portas e
            janelas.
          </InstructionsTop>
          <InstructionsBottom>
            <ul>
              <li>
                Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50
                metros quadrados, mas podem possuir alturas e larguras
                diferentes
              </li>
              <li>
                O total de área das portas e janelas deve ser no máximo 50% da
                área de parede
              </li>
              <li>
                A altura de paredes com porta deve ser, no mínimo, 30
                centímetros maior que a altura da porta
              </li>
              <li>Cada janela possui as medidas: 2,00 x 1,20 mtos </li>
              <li>Cada porta possui as medidas: 0,80 x 1,90</li>
              <li>Cada litro de tinta é capaz de pintar 5 metros quadrados</li>
              <li>Não considerar teto nem piso</li>
              <li>
                As variações de tamanho das latas de tinta são: 0,5 L - 2,5 L -
                3,6 L - 18 L
              </li>
            </ul>
          </InstructionsBottom>
        </InstructionsBox>
        <InputsBox>
          <Form onSubmit={handleSubmit(handleSubmitArea)}>
            <InputsDiv>
              <InputsTop>
                <Text>
                  <span>Parede 1</span>
                </Text>
              </InputsTop>
              <InputsTop>
                Altura
                <InputField
                  placeholder="metros"
                  {...register("height1", { value: "0,00" })}
                />
              </InputsTop>
              <InputsTop>
                Largura
                <InputField
                  placeholder="metros"
                  {...register("width1", { value: "0,00" })}
                />
              </InputsTop>
              <InputsTop>
                Portas
                <InputField
                  placeholder="unidades"
                  {...register("doors1", { value: 0 })}
                />
              </InputsTop>
              <InputsTop>
                Janelas
                <InputField
                  placeholder="unidades"
                  {...register("windows1", { value: 0 })}
                />
              </InputsTop>
            </InputsDiv>
            <InputsDiv>
              <Text>Parede 2</Text>
              <InputField
                placeholder="metros"
                {...register("height2", { value: "0,00" })}
              />
              <InputField
                placeholder="metros"
                {...register("width2", { value: "0,00" })}
              />
              <InputField
                placeholder="unidades"
                {...register("doors2", { value: 0 })}
              />
              <InputField
                placeholder="unidades"
                {...register("windows2", { value: 0 })}
              />
            </InputsDiv>
            <InputsDiv>
              <Text>Parede 3</Text>
              <InputField
                placeholder="metros"
                {...register("height3", { value: "0,00" })}
              />
              <InputField
                placeholder="metros"
                {...register("width3", { value: "0,00" })}
              />
              <InputField
                placeholder="unidades"
                {...register("doors3", { value: 0 })}
              />
              <InputField
                placeholder="unidades"
                {...register("windows3", { value: 0 })}
              />
            </InputsDiv>
            <InputsDiv>
              <Text>Parede 4</Text>
              <InputField
                placeholder="metros"
                {...register("height4", { value: "0,00" })}
              />
              <InputField
                placeholder="metros"
                {...register("width4", { value: "0,00" })}
              />
              <InputField
                placeholder="unidades"
                {...register("doors4", { value: 0 })}
              />
              <InputField
                placeholder="unidades"
                {...register("windows4", { value: 0 })}
              />
            </InputsDiv>
            <Button type="submit">Calcular</Button>
          </Form>
          <ResultDiv>
            {totalMeters > 0 ? (
              <>
                <div>
                  A área total a ser pintada é de {totalMeters.toFixed(2)} m²
                </div>
                <div>Você irá precisar de {totalCans}</div>
              </>
            ) : (
              ""
            )}
          </ResultDiv>
        </InputsBox>
      </Container>
      <Footer>
        <a href="https://br.freepik.com/fotos-vetores-gratis/vista-de-cima">
          Vista de cima foto criado por freepik - br.freepik.com
        </a>
      </Footer>
    </>
  );
};

export default Home;
