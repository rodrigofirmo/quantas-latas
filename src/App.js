import { BrowserRouter, Routes, Route } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import Home from "./pages/Home";
import { GlobalStyles, Reset, RootVariables } from "./styles";

const App = () => {
  return (
    <>
      <Reset />
      <RootVariables />
      <GlobalStyles />
      <ToastContainer style={{ padding: "15px", zIndex: 99999 }} />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
